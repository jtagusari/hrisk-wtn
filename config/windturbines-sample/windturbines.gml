<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ windturbines.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>141.1296892490689</gml:X><gml:Y>43.23269713756249</gml:Y><gml:Z>100</gml:Z></gml:coord>
      <gml:coord><gml:X>141.2903685531752</gml:X><gml:Y>43.28220420881544</gml:Y><gml:Z>100</gml:Z></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                      
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.0">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>141.129689249069,43.2326971375625,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>141.239555439911,43.2472033773598,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:windturbines fid="windturbines.2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:4326"><gml:coordinates>141.290368553175,43.2822042088154,100</gml:coordinates></gml:Point></ogr:geometryProperty>
    </ogr:windturbines>
  </gml:featureMember>
</ogr:FeatureCollection>
