[ README in English ](/README-en.md)

# H-RISK <img src="config/hexicon.png" width = 100>

## About

- 風車騒音による健康リスクを評価するためのソフトウェア「H-RISK」
- 洋上・陸上を問わず騒音の予測が可能ですが，騒音のレベルに影響を与える様々な因子のうちの一部（幾何減衰・空気吸収・地表面反射）しか考慮されておらず，例えば地形や障害物の影響，風や温度による音の屈折等は反映されません。予測手法や適用可能範囲についての詳細はマニュアル中の「風車騒音の伝搬計算方法について」を参照してください。
- 風車のごく近傍（~500m程度）のリスクは，デフォルトの設定では，過大あるいは過小に評価される可能性があります。（将来対応予定）

## インストーラー・マニュアルのダウンロード

Windows 10 / Windows 11用のインストーラー・マニュアルは以下のリンクからダウンロードできます。


インストーラー最新版（バージョン 0.5.0）

<https://drive.google.com/file/d/1RVKGXUBDeMJzXmf4peEg20tfrnKjIOGf/view?usp=sharing>

ハッシュ値（SHA256）：7dc71e3b7df6ee3b8a3ba5e7a5d82fac8067d8e25b532ec5852c7f41132c7dff

マニュアル

<https://drive.google.com/file/d/1p0F48yV1PhR8GeNNiBVPdujPsJeiFLQa/view?usp=sharing>

## インストール手順

上記URLからインストーラーをダウンロードし，PC (Windows 10 / Windows 11) で実行すればインストールできます。
なお，このソフトウェアには，悪意のあるプログラムは含まれませんが，ディジタル証明書が付属していないため，インストール時にセキュリティの警告が出ます。
最終的には，インストールの実行について，各ユーザーが判断してください。


## やや高度な内容

### ハッシュ値によるファイル同一性の確認

インストーラーのハッシュ値（SHA256）は，インストーラーが改変されていないことを確認するのに使うことができます。
確認をしたい場合には，Windowsコマンドプロンプトで`certutil -hashfile ＜ダウンロードしたインストーラー＞ SHA256`を行い，上記のハッシュ値と同じ値かどうか比較してください。


### 開発者用メモ

- H-RISKのスクリプトは，このリポジトリで全て公開されています。
- 開発者向けの詳細は`www/ja/rmd/dev.Rmd`を参照してください。
- スクリプトを開発環境で実行するためには，別途，`R (version 4.0.5)`および`RStudio`のインストールが必要です。
- スクリプトからインストーラーを作成するためには，`R-Portable`フォルダに`R Portable (version 4.0.5)`のインストールが必要です。


## 過去のバージョン

- 0.3.4

   <https://drive.google.com/file/d/1fhSaszCfWaZgOAh8FqthpceNL1uU5UrJ/view?usp=sharing>

   ハッシュ値（SHA256）：5024a6be380a31a168a70ae5780b63ddddc55b58551afdc9da0530b8d51ef70c
- 0.4.6

   <https://drive.google.com/file/d/113m2uH4-0k3D-w6zKlV36QL-LkJE5AWW/view?usp=sharing>

   ハッシュ値（SHA256）：9aace9895f231b4cbab1d9a24c0942c56a30c467bae49ec184417fbf527cb2c9
