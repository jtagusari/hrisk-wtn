# Calculation conditions

The calculation conditions are shown below.
See `r save_path$SPL_CALC` for details.

## Setting of weather conditions

- Temperature: `r SPL_CALC$TEMPERATURE`°C
- Humidity: `r SPL_CALC$HUMIDITY`%.
- Air pressure: `r SPL_CALC$ATOMPRESSURE` hPa

## Setting of geometries

```{r echo = F}
max_buf_str <- if_else(
  SPL_CALC$MAXBUF_FROM_WT == 0,
  sprintf("%.1f km（自動設定）", SPL_CALC$AUTO_MAXBUF$MAXBUF/1000),
  sprintf("%.1f km（指定）", SPL_CALC$MAXBUF_FROM_WT/1000)
)
```

- Calculation range: Radius `r max_buf_str` from each windmill
- Calculation point: Center point of the mesh that divides `r SPL_CALC$PRECISION[1]` horizontally and vertically into a 5th order mesh (about 250m x 250m) within the calculation range
- Coordinate system: Universal Transverse Mercator (EPSG: `r sprintf("%d",crs_xy)`)[^EPSG])

[^EPSG]: EPSG code to identify the reference coordinate system.

## Others

-   Date and time：`r Sys.time()`
-   Version of the software：`r GENERAL$HRISK_VERSION`
-   Hash[^hash]：`r CALC_HASH`

[^hash]: md5 hash value issued according to calculation conditions and results. It can be obtained by `tools::md5sum("wtn-vars.RDS")` or an appropriate md5 hash generation function. It can be used to check the identity of calculation results.
