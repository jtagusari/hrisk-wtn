// packageの読み込み
const {app, BrowserWindow, dialog} = require('electron')
const pjson = require('../package.json');
const path = require('path');
const log = require('electron-log')
const child = require('child_process');
const iconv = require("iconv-lite");
const os = require("os");
const fs = require("fs");
// const { session } = require('electron');

const app_lock = app.requestSingleInstanceLock();
if (!app_lock) {
  log.info("App is already running");
  app.quit();
}

// app.getAppPathで取れるパス（通常resources/app）を取得
// 通常実行時 package.jsonが置かれているフォルダ
// コンパイル時 resources/app
// asar = T だと resources/app.asarとなるが，asar = F のためにappはフォルダとなる．
var appPath = app.getAppPath();

// logfileのパスの指定
var tempPath = fs.mkdtempSync(path.join(os.tmpdir(), 'hrisk-'));
var logPath = path.join(tempPath, "hrisk.log");

log.transports.file.format = '{y}-{m}-{d} {h}:{i}:{s} | {text}';
log.transports.console.format = '{h}:{i}:{s} | {text}';
log.transports.file.resolvePath = (variables) => {
  return logPath;
};

// log用の区切り
var logBreak = '************************************************************'

// app名称とバージョン
log.info(logBreak);
log.info(pjson.name + " " + pjson.version);
log.info(pjson.description);
log.info(logBreak);
log.info("")

// log開始，apppathを記録
log.info(logBreak);
log.info('Configurations');
log.info(logBreak);
log.info('App: '+ appPath);
log.info('Temp: '+ tempPath);
log.info(('Log: '+ logPath).replace(tempPath,'%TempPath%'));


// RScript.exeが置かれているpathの設定
var rPath = path.join(appPath, "R-Portable", "App", "R-Portable", "bin", "RScript.exe" );
log.info(('Rscript: '+ rPath).replace(appPath,'%AppPath%'));

// Rライブラリのpath
var rLibPath = path.join(appPath, "R-library" );
log.info(('R-lib: '+ rLibPath).replace(appPath,'%AppPath%'));

// pandoc.exeが置かれているpathの設定(dirまで)
var pandocPath = path.join(appPath, "pandoc");
log.info(('Pandoc: '+ pandocPath).replace(appPath,'%AppPath%'));

// htmlが置かれているpathの設定
var electronPath = __dirname;
var htmlPath = path.join(__dirname, "now-loading.html");
log.info(('main.js: '+ electronPath).replace(appPath,'%AppPath%'));
log.info(('preloadHTML: '+ htmlPath).replace(electronPath,'%mainjsPath%'));

// pathを整形
appPath = appPath.replace(/\\/g, "/"); // R用に
tempPath = tempPath.replace(/\\/g, "/"); // R用に
rPath = rPath.replace(/\\/g, "\\\\"); // windows用に
rLibPath = rLibPath.replace(/\\/g, "/"); // R用に
pandocPath = pandocPath.replace(/\\/g, "/"); // R用に
htmlPath = htmlPath.replace(/\\/g, "\\\\"); // windows用に

// アプリを実行するportとlocalURL
const port = "9191";
const localURL = 'http://127.0.0.1:'+port
log.info('AppURL: '+localURL);

// エラーのときのためのダイアログ
var showErrDialog = function(message){
  dialog.showErrorBox(
    'エラー', 
    message+'\nエラーの詳細はログファイル（'+logPath+'）を確認してください．'
    );
}


app.whenReady().then(() =>{

/*  // cspの設定をやろうとしたが，うまいやり方がわからないのでコメントアウト．
  session.defaultSession.webRequest.onHeadersReceived((details, callback) => {
    callback({
      responseHeaders: {
        ...details.responseHeaders,
        'Content-Security-Policy': [
          'default-src \'self\''
        ]
      }
    })
  })
  */
  // BrowserWindowインスタンスを生成する
  mainWindow = new BrowserWindow({
    width: 1366,
    height: 768,
    autoHideMenuBar: true,  
    /*webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }*/
  });

  // logウインドウを表示する場合には以下およびlogWindow関連のcommandを入れる
/*  logWindow = new BrowserWindow({
    width: 640,
    height: 480,
    autoHideMenuBar: true,  
    title: "LOG: "+logPath,
    closable: false
  });*/



  // アプリが立ち上がるまでの画面を読み込み
  mainWindow.loadURL('file://'+htmlPath);// + '?logPath=' + logPath);
  //mainWindow.webContents.openDevTools(); //DevToolを立ち上げる

  /*logWindow.loadURL('file://'+logPath);*/

  // Rを実行する
  // LocaleはWindowsでのShift-JISに対応している．
  // 初回，ja_JP.UTF8@cjknarrowについての警告が出る．解決方法不明．
  log.info(logBreak);
  log.info('R command execution for app preparation')
  log.info('(Note: Warning for LC_CTYPE is unavioidable)')
  log.info(logBreak);
  const rShinyProcess = child.spawn(
    rPath, 
    [
      "--vanilla",
      "-e", "setwd('"+appPath+"')" ,
      "-e", "TEMP_DIR <- {'"+tempPath+"'}", // middle blacket is essential!
      "-e", "locale <- suppressMessages(Sys.setlocale('LC_ALL', 'ja'))",
      "-e", ".libPaths('" + rLibPath + "')", 
      "-e", "Sys.setenv(RSTUDIO_PANDOC='" + pandocPath + "')", 
      "-e", "library(shiny, quiet = T)", 
      "-e", "shiny::runApp(port="+port+")"
    ]
    );  
  rShinyProcess.stdout.on('data', (data) => {
    log.info(`${data}`);
  });


  // Rからの出力を読み取り，"Listening on"が見つかったらURLを移行する
  // =>はアロー関数で，(引数) => {関数}のかたちをとっている
  rShinyProcess.stderr.on('data', (data) => {
    var msg = iconv.decode(data, "windows-31j").split(/\r\n/).filter((item) => item != '');
    msg.map((item) => log.info(item));
    /*logWindow.loadURL('file://'+logPath);*/
    if (data.indexOf("Listening on") > -1) {
      log.info(logBreak);
      log.info('Application execution');
      log.info(logBreak);
      mainWindow.loadURL(localURL);
    } else if(data.indexOf("Execution halted") > -1 | data.indexOf("Error in") > -1){
      showErrDialog(data);
    }
  });

  mainWindow.on('close', () =>{
    try{
/*      logWindow.destroy();*/
      process.kill(rShinyProcess.pid);
    } catch {};
    app.quit()
  })
  app.on('window-all-closed', () => {app.quit()})

})