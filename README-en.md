[ 日本語版 README ](/README.md)

# H-RISK <img src="config/hexicon.png" width = 100>

## Note

The most text written below is just translated from Japanese and may be wrong.
If you any questions, please do not hesitate to contact the developer.

## About

- This software, named "H-RISK", is for assessing health risks from wind turbine noise
- It can predict noise both offshore and onshore, but it only takes into account a part of the various factors that affect the noise level (geometric attenuation, air absorption, and ground reflection) and does not reflect, for example, the effects of topography and obstacles, or sound refraction by wind and temperature. Please refer to "Calculation Methods for Wind Turbine Noise Propagation" in the manual for details on the prediction method and its applicability.
- Risks in the very vicinity of wind turbines (~500m) may be overestimated or underestimated with the default settings (to be addressed in the future).


## Download installer

The installer for Windows 10 / Windows 11 can be downloaded from the following links.



The latest installer (version 0.4.5-en）

<https://drive.google.com/file/d/17m__BA6vkivwoEUQBpW1lcjNCNXrJbV1/view?usp=sharing>

Hash（SHA256）：686f1eeb6ca86913270af1b6e33266c77c0b9ba0be56bb4366a1bc58212c2962


## How to Install

Download the installer from the above URL and run it on your PC (Windows 10 / Windows 11) to install the software.
This software does not contain any malicious programs, but since it does not come with a digital certificate, a security warning will appear during installation.
Ultimately, it is up to each user to decide whether or not to perform the installation.


## Somewhat advanced content

### About English version

This software is originally developed in Japanese.

The `main` branch always represents the Japanese version.
Please see `main-en` branch that represents the English version.

Regarding the calculation, the scripts in the English version are the same as the Japanese version, except for the configuration files that are stored in `config-en` folder intead of `config` folder.
See the definition of `findConfigPath` function in the `config.R` file.

Regarding the translation, the following files were modified:

- `README.md` file was translated as `README-en.md` file
- files in `config` folder were translated and stored in `config-en` folder
- files in `www/ja` folder were translated and stored in `www/en` folder

If you want to modify the translations, edit these files.

### Confirmation of file identity by hash value

The hash value of the installer (SHA256) can be used to verify that the installer has not been modified.
To check, run `certutil -hashfile <downloaded installer> SHA256` at the Windows command prompt and compare if the value is the same as the hash value above.

### For developers

- All scripts of "H-RISK" are available in this repository.
- See `www/en/rmd/dev.Rmd` for details for developers.
- To run the scripts in a development environment, you need to install `R (version 4.0.5)` and `RStudio` separately.
- To create an installer from the script, you need to install `R Portable (version 4.0.5)` in the `R-Portable` folder.


