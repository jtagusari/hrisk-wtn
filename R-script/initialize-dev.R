# Initialization
message("R initialization for developer")

# Check R version
# 4.0.5 is required 
message("Checking R version")

if(version$major != "4" | version$minor != "0.5"){
  message("R version 4.0.5 is required! Change settings and restart session!")
} else {
  message("OK! R 4.0.5 is running")
  
  # Temporary path is input as a argument
  message("Setting temporary directory (if it has not been created with Node.js)")
  
  # create directory for executing this in R
  if(!exists("TEMP_DIR")){
    TEMP_DIR <- gsub("\\\\","/",gsub("file","",tempfile()))
    dir.create(TEMP_DIR, showWarnings = F)
  }  
  
  message(paste0("TempPath (R-format): ", TEMP_DIR))
  
  # Set library path
  message("Setting library path")
  .libPaths(paste0(getwd(), "/R-library"))
   
  
}

# This is the setting for the required packages
# execute below lines if you want update required packages as the currently installed packages

# library(tidyverse)
# library(pacman)
# library(jsonlite)
# 
# info_packages <- p_lib() %>%
#   purrr::map(~p_info(.x)[c("Package","Version", "Title", "Repository", "Description", "Built")])
# 
# info_packages_df <- purrr::map_dfr(info_packages, ~tibble(pkg = .x$Package, ver = .x$Version))
# 
# write_json(info_packages, "config/r-packages.json", pretty = T, auto_unbox = T)
