# function to estimate 'bothering' from the parameter obtained from Nakamura's experiment
# estBotheringProb <- function(level, freq_num){
#   par <- dplyr::filter(RISK_EST$BOTHERING_NAKAMURA_PAR, freq == freq_num)
#   if(nrow(par) == 0) {
#     return(NA)
#   } else {
#     return(plogis(par$beta0 + par$beta1 * level))
#   }
# }

# Estimate the population affected by WtN ---------------------------------

estRiskFromRaster <- function(ras_result){
  
  # 1. Define a local function to sum up the risk
  estimate_risk <- function(outcome_attr){
    est_args <- outcome_attr$estimate_args
    if(all(purrr::map_lgl(est_args, ~.x %in% names(ras_result)))){
      est_func <- eval(parse(text = outcome_attr$estimate_func)) %>% as_mapper()
      outcome_attr$estimate <- do.call(est_func, purrr::map(est_args, ~ras_result[[.x]]))
    } else {
      return()
    }
    
    return(outcome_attr)
  }
  
  # estimate the risk
  risk_est_results <- RISK_PROPERTIES %>% 
    purrr::map(~purrr::modify_at(.x, "outcome", ~purrr::map(.x, ~estimate_risk(.x)))) %>% 
    purrr::map(~purrr::modify_at(.x, "outcome", ~purrr::compact(.x))) %>% 
    purrr::map(~purrr::modify_at(.x, "outcome", 
                                 ~transpose(.x) %>% 
                                   as_tibble() %>% 
                                   tidyr::unnest(c(matches("label|estimate$"),ends_with("_note"))) %>%
                                   dplyr::select(c(matches("label|estimate$"),ends_with("_note")))
                                 ))


  return(risk_est_results)
}

# make the risk table using gt package

makeRiskTable <- function(risk_est_results){

  if(is.null(risk_est_results)) return()

  df_risk <- risk_est_results$outcome %>%
    dplyr::select(-ends_with("_note")) %>% 
    setNames(c(risk_est_results$outcome_colname, risk_est_results$estimate_colname))

  gt_risk <- gt(df_risk) %>% 
    tab_header(risk_est_results$title) %>% 
    fmt_number(
      columns = risk_est_results$estimate_colname,
      decimals = 0,
      use_seps = T, sep_mark = " "
    ) %>% 
    tab_options(
      table.font.size = GENERAL$RISK_TAB_FONTSIZE
    ) 
  
  df_note_cells <- bind_rows(
    tibble(label_note = "", estimate_note = "")[0,],
    risk_est_results$outcome 
  ) %>% 
    dplyr::select(ends_with("_note"))
  
  risk_notes <- 
    # footnote for columns
    tribble(
      ~footnote,                         ~location,
      risk_est_results$outcome_colnote,  cells_column_labels(risk_est_results$outcome_colname),
      risk_est_results$estimate_colnote, cells_column_labels(risk_est_results$estimate_colname),
    ) %>% 
    # footnote for label cells
    bind_rows(
      tibble(
        footnote = df_note_cells$label_note,
        location = purrr::map(1:nrow(df_note_cells), 
                              ~cells_body(risk_est_results$outcome_colname, .x))
      )
    ) %>% 
    # footnote for estimates
    bind_rows(
      tibble(
        footnote = df_note_cells$estimate_note,
        location = purrr::map(1:nrow(df_note_cells), 
                              ~cells_body(risk_est_results$estimate_colname, .x))
      )
    ) %>% 
    dplyr::filter(footnote != "") %>% 
    dplyr::group_by(footnote) %>% 
    dplyr::summarise(locations = list(location))
  
  if(nrow(risk_notes) > 0){
    for(i in 1:nrow(risk_notes)){
      gt_risk <- tab_footnote(gt_risk,risk_notes$footnote[i], risk_notes$locations[[i]])
    }
  }
      
  return(gt_risk)
    
}
