
# defined  but not used
cmptValueArea <- function(v, v_grid, dx = 1, dy = 1, v_grid){
  
  func_output <- function(v){
    v_grid_sort <- sort(v_grid)
    v_grid_order <- order(v_grid)
    if (v >= v_grid_sort[4]) {
      return(0)
    } else if (v <= v_grid_sort[1]) {
      return(dx * dy)
    } else {
      x1 <- (v - v_grid[1]) / (v_grid[2] - v_grid[1]) * dx
      x2 <- (v - v_grid[3]) / (v_grid[4] - v_grid[3]) * dx
      y1 <- (v - v_grid[1]) / (v_grid[3] - v_grid[1]) * dy
      y2 <- (v - v_grid[2]) / (v_grid[4] - v_grid[2]) * dy
      
      if (v >= v_grid_sort[3]) {
        case_when(
          v_grid_order[4] == 1L ~ S <- x1 * y1 / 2,
          v_grid_order[4] == 2L ~ S <- (dx - x1) * y2 / 2,
          v_grid_order[4] == 3L ~ S <- x2 * (dy - y1) / 2,
          v_grid_order[4] == 4L ~ S <- (dx - x2) * (dy - y2) / 2
        )
      } else if (v >= v_grid_sort[2]) {
        v_grid_order_34 <- paste(v_grid_order[3:4], collapse = "")
        case_when(
          v_grid_order_34 == "12" ~ S <- (y1 + y2) * dx / 2,
          v_grid_order_34 == "13" ~ S <- (x1 + x2) * dy / 2,
          v_grid_order_34 == "14" ~ S <- x1 * y1 / 2 + x2 * y2 / 2,
          v_grid_order_34 == "23" ~ S <- (dx - x1) * y2 / 2 + x2 * (dy - y1) / 2,
          v_grid_order_34 == "24" ~ S <- (dx - x1 + dx - x2) * dy / 2,
          v_grid_order_34 == "34" ~ S <- (dy - y1 + dy - y2) * dx / 2
        )
      } else if (v >= v_grid_sort[1]) {
        case_when(
          v_grid_order[1] == 1L ~ S <- dx * dy - x1 * y1 / 2,
          v_grid_order[1] == 2L ~ S <- dx * dy - (dx - x1) * y2 / 2,
          v_grid_order[1] == 3L ~ S <- dx * dy - x2 * (dy - y1) / 2,
          v_grid_order[1] == 4L ~ S <- dx * dy - (dx - x2) * (dy - y2) / 2
        )
      } else {
        S <- as.double(NA)
      }
      return(S)
    }
  }
  
  return(func_output)
}
