# Initialization
message("R initialization for developer")

# Check R version
# 4.0.5 is required 
message("Checking R version")

if(version$major != "4" | version$minor != "0.5"){
  message("R version 4.0.5 is required! Change settings and restart session!")
} else {
  message("OK! R 4.0.5 is running")
  
  # Temporary path is input as a argument
  message("Setting temporary directory (if it has not been created with Node.js)")
  
  # create directory for executing this in R
  if(!exists("TEMP_DIR")){
    TEMP_DIR <- gsub("\\\\","/",gsub("file","",tempfile()))
    dir.create(TEMP_DIR, showWarnings = F)
  }  
  
  message(paste0("TempPath (R-format): ", TEMP_DIR))
  
  
  # load pacman and tidyverse
  # if there are no packages installed, install from CRAN
  message("Installing R libraries, if necessary")
  library(utils)
  suppressMessages({
    if (!require("pacman")) install.packages("pacman", reops="https://cran.ism.ac.jp/")
    if (!require("tidyverse")) pacman::p_install_version("tidyverse", "1.3.1")
    if (!require("jsonlite")) pacman::p_install_version("jsonlite", "1.7.2")
    library(tidyverse)
    library(jsonlite)
  })
  
  # check currently installed packages and required packages
  # their versions are also important
  df_packages <- 
    read_json("config/r-packages.json", simplifyVector = T) %>% 
    bind_rows() %>% 
    dplyr::mutate(Version_required = Version) %>% 
    dplyr::left_join(
      purrr::map_dfr(p_lib(), ~tibble(Package = .x, Version_installed = p_info(.x)$Version)),
      by = "Package"
    )%>% 
    dplyr::mutate(Version_match = Version_installed == Version_required) 
  
  # install required packages, if they aren't installed
  if(sum(!df_packages$Version_match) > 0){
    df_packages %>%
      dplyr::filter(!Version_match) %>%
      dplyr::transmute(package = Package, version = Version_required) %>%
      purrr::pmap(p_install_version)
  } else {
    message("All required libraries are installed")
  }
  
  
  # remove the information about the packages
  rm(df_packages)
  
  
  
}

# This is the setting for the required pacakges
# execute below lines if you want update required packages as the currently installed packages

# library(tidyverse)
# library(pacman)
# library(jsonlite)
# 
# info_packages <- p_lib() %>%
#   purrr::map(~p_info(.x)[c("Package","Version", "Title", "Repository", "Description", "Built")])
# 
# write_json(info_packages, "config/r-packages.json", pretty = T, auto_unbox = T)
