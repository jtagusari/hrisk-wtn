# icons for indicating the locations of the Wts
makeWtIcon <- function(wt_data = NA, icon_url = NA, size = NA){
  if(is.na(icon_url)) icon_url <- WT$MARKER$ICON_PATH$WHITE
  if(is.na(size)){
    if(!is.na(wt_data) && !is.na(wt_data$epower)) {
      size <- sqrt(wt_data$epower)*1e-2
    } else {
      size <- 0.8
    }
  }
  
  # lower and upper limit of the size
  size <- if_else(size > 1.5, 1.5, if_else(size < 0.5, 0.5, size))
  
  return(
    leaflet::makeIcon(
      iconUrl = icon_url ,
      iconWidth = 50*size, iconHeight = 60*size,
      iconAnchorX = 25*size, iconAnchorY = 60*size
    )
  )
}


# Get the EPSG code for the UTM coordinates fit to a specific geometry setting -----------------------------

getEPSGcodeUTM <- function(longtitude = 140, latitude = 35){
  longtitude <- sign(longtitude) *  (((abs(longtitude) + 180.0) %% 360.0) - 180.0)
  epsg_code <- CRS_LIB %>% 
    dplyr::filter(
      lng_min <= longtitude,
      lng_max > longtitude,
      lat_min <= latitude,
      lat_max > latitude
    ) %>% 
    .$epsg_code
  return(epsg_code)
}


# Initiallize the map -------------------------------------------------

initMap <- function(){
  
  init_long <- GENERAL$INITIAL_LOCATION$LONG
  init_lat  <- GENERAL$INITIAL_LOCATION$LAT
  init_zoom <- GENERAL$INITIAL_LOCATION$ZOOM
  
  easyprint_str <- 
  "function(el, x) {
    L.easyPrint({
      sizeModes: ['Current', `A4Landscape`],
      filename: 'risk-map',
      exportOnly: true,
      hideControlContainer: false
    }).addTo(this);
   }"
  
  map <- leaflet() %>%
    addTiles() %>%
    setView(lng = init_long, lat = init_lat, zoom = init_zoom) %>%
    addScaleBar("bottomright", scaleBarOptions(imperial = F)) %>% 
    onRender(easyprint_str)
  
  return(map)
}


# get Wt geometry when clicked the map ------------------------------------

initWtFromMapClick <- function(lf_click, wt_data_exist, ...){
  
  if (!inherits(wt_data_exist, "data.frame")) {
    return(initWt())
  }
  
  wt_id_new <- setdiff(
    setWtId(seq(1, nrow(wt_data_exist) + 1)),
    wt_data_exist$wt_id
  ) %>% 
    .[1]
  
  lng_modified <- sign(lf_click$lng) *  (((abs(lf_click$lng) + 180.0) %% 360.0) - 180.0)

  wt_new <- do.call(
    initWt,
    c(
      list(
        wt_id          = wt_id_new,
        lng            = lng_modified, 
        lat            = lf_click$lat,
        LW_lib_id_init = if_else(
          input[["LW_edit_panel_type_rbtn"]]=="auto", "auto", 
          input[["LW_edit_panel_lib_dropdown"]]
          )
      ),
      list(...)
    )
  )
  
  return(wt_new)
}

# Add Wt to the map -------------------------------------------------------

addWtToMap <- function(lf, lf_bound = NULL, wt_data, wt_icon = NULL, map_zoom = 9, map_bounds = NULL){

  if (
    inherits(wt_data, "data.frame") && nrow(wt_data) > 0
    ) {
    
    if (is.null(wt_icon)) wt_icon <- makeWtIcon(wt_data)
  
    lf_wt <- lf %>% 
      removeMarker(layerId = wt_data$wt_id) %>% 
      addMarkers(
        layerId      = wt_data$wt_id,
        lng          = wt_data$lng,
        lat          = wt_data$lat,
        label        = wt_data$label %>% purrr::map(~gt::html(.x)),
        icon         = wt_icon,
        labelOptions = WT$MARKER$LABEL_OPTIONS
      )
    
    if (!is.null(map_bounds)) {
      if (map_bounds$west < -180.0) {
        bias <- (-((-map_bounds$west + 180.0) %% 360.0) + 180.0) - map_bounds$west
        
        lf_wt <- fitBounds(lf_wt, 
                         lng1  = map_bounds$west + bias, lng2 = map_bounds$east + bias, 
                         lat1  = map_bounds$south, lat2 = map_bounds$north)
      } else if (map_bounds$east > 180.0) {
        bias <- (((map_bounds$east + 180.0) %% 360.0) - 180.0) - map_bounds$east
        
        lf_wt <- fitBounds(lf_wt, 
                           lng1  = map_bounds$west + bias, lng2 = map_bounds$east + bias, 
                           lat1  = map_bounds$south, lat2 = map_bounds$north)
      }
    }
    
    if (!is.null(lf_bound)) {
      is_inside_bound <- purrr::map2_lgl(
        wt_data$lng, wt_data$lat,
        ~all(
          .x >= lf_bound$west, 
          .x <= lf_bound$east, 
          .y >= lf_bound$south, 
          .y <= lf_bound$north
          )
      ) %>% any()
    } else if (is.null(lf_bound) && nrow(wt_data) > 1) {
      is_inside_bound <- FALSE
    } else {
      is_inside_bound <- TRUE
    }
      
    if(!is_inside_bound){
      lf_wt <- setView(lf_wt, 
              lng  = mean(wt_data$lng), 
              lat  = mean(wt_data$lat),
              zoom = map_zoom)
    }
    
    return(lf_wt)
    
  } else {
    
    message("Wt data-frame object is not given")
    return(lf)
    
  }
}


# Add calculation areas to the map ----------------------------------------
addCalcAreaToMap <- function(lf, sf_calc_area){
  
  if (inherits(sf_calc_area, "sf") &&
      all(st_geometry_type(sf_calc_area)  %in% c("POLYGON", "MULTIPOLYGON"))
  ) {
    lf_calcarea <- lf %>% 
      addPolylines(data = st_transform(sf_calc_area, 4326), weight = 3, 
                   color = CONTOUR$CALC_AREA$colour, label = CONTOUR$CALC_AREA$label) %>%
      addLegend(labels = CONTOUR$CALC_AREA$label, colors = CONTOUR$CALC_AREA$colour,
                opacity = 1, position = "topleft")
  } else {
    message("Sf polygon object is not given")
    lf_calcarea <- lf
  }
  
  return(lf_calcarea)
  
}

# Add buffer lines to the map ----------------------------------------
addBufsToMap <- function(lf, wt_data, bufs = -1.0, use_bbox = FALSE){
  
  crs_xy <- getEPSGcodeUTM(mean(wt_data$lng), mean(wt_data$lat))
  
  if (inherits(wt_data, "data.frame") &&
      "lat" %in% colnames(wt_data) && "lng" %in% colnames(wt_data)
  ) {
    wt_geom <- st_as_sf(wt_data, coords = c("lng", "lat"), crs = crs_xy)
    bufs <- bufs[bufs > 0]
    
    if (all(bufs < 0)) {
      return(lf)
    }

    if (use_bbox) {
      wt_bufs_geom <- purrr::map_dfr(
        bufs, 
        ~wt_geom %>% 
          st_buffer(.x) %>% 
          st_bbox() %>%
          st_as_sfc() %>%
          st_cast("LINESTRING") %>%
          st_as_sf() %>% 
          dplyr::mutate(buf = sprintf("%d m", .x))
        ) 
    } else {
      wt_bufs_geom <- purrr::map_dfr(
        bufs, 
        ~wt_geom %>% 
          st_buffer(.x) %>% 
          dplyr::summarise() %>% 
          dplyr::mutate(buf = sprintf("%d m", .x))
        ) 
    }


    lf_bufs <- lf %>% 
      addPolylines(data = st_transform(wt_bufs_geom, 4326),
                    weight = 3, color = CONTOUR$BUFFER_AREA$colour, 
                    label = ~paste0(CONTOUR$BUFFER_AREA$label, ": ", buf),
                    labelOptions = CONTOUR$labelOptions)
  } else {
    message("Wt data is not valid")
    lf_bufs <- lf
  }
  
  return(lf_bufs)
}




# Add populations to the map ----------------------------------------------
# leafem package is used
addPopRasterToMap <- function(lf, ras_pop){
  
  if (inherits(ras_pop, "stars")) {
    
    val <- ras_pop$pop
    pal <- colorNumeric(CONTOUR$POPULATION$palette, val, na.color = "transparent")
    
    lf_pop <- lf %>% 
      addStarsImage(ras_pop, opacity = 0.5, colors = pal, layerId = "pop") %>%
      addImageQuery(ras_pop, prefix  = CONTOUR$POPULATION$label, layerId = "pop") %>% 
      addLayersControl(overlayGroups = "pop") %>% 
      addLegend(pal = pal, values = val, title = CONTOUR$POPULATION$label, position = "bottomleft")
  
  } else {
    message("Stars raster object is not given")
    lf_pop <- lf
  }
  
  return(lf_pop)
  
}

# Add contours to the map -------------------------------------------------
addContourToMap <- function(lf, contour_to_plot, app_control){
  
  if (!inherits(contour_to_plot, "list")) {
    message("List that contains sf objects is not given")
    return(lf)
  }
  
  if (is.null(contour_to_plot$plg) && is.null(contour_to_plot$lin)) {
    message("Contour objects are NULL")
    return(lf)
  }
  
  func_plg <- function(lf){
    
    contour_geom <- contour_to_plot$plg 
    contour_unique <- as_tibble(contour_geom) %>% 
      dplyr::distinct(value, label, fill)
    
    legend_lab <- factor(contour_unique$value, label = contour_unique$label) %>% levels()
    legend_col <- factor(contour_unique$value, label = contour_unique$fill) %>% levels()
    legend_symb <- purrr::map(
      legend_col,
      ~makeSymbol(
        shape = CONTOUR$LEGEND$plg$shape,
        width = 24,
        fillColor = .x,
        color = "black"
        )
    )
    legend_sty <- purrr::imap_chr(
      CONTOUR$LEGEND$plg$labelStyle, 
      ~paste0(.y, ":", .x)
      ) %>% 
      paste(collapse = ";")
    
    lf %>% 
      clearImages() %>% 
      removeLayersControl() %>% 
      addPolygons(data = st_transform(contour_geom, 4326), 
                  label = ~label,weight = 0.5, color = ~colour,
                  fillColor = ~fill, fillOpacity = app_control$fill_opacity,
                  labelOptions = CONTOUR$labelOptions) %>% 
      addLegendImage(
        title = htmltools::tags$div(contour_to_plot$title, style = legend_sty),
        images = rev(legend_symb),
        width = CONTOUR$LEGEND$plg$width,
        height = CONTOUR$LEGEND$plg$height,
        labels = rev(legend_lab),
        position = CONTOUR$LEGEND$plg$position,
        labelStyle = legend_sty
      )
  }
  
  func_lin <- function(lf){
    contour_geom <- contour_to_plot$lin
    contour_unique <- as_tibble(contour_geom) %>% 
      dplyr::distinct(value, label, colour)
    
    legend_lab <- factor(contour_unique$value, label = contour_unique$label) %>% levels()
    legend_col <- factor(contour_unique$value, label = contour_unique$colour) %>% levels()
    legend_symb <- purrr::map(
      legend_col,
      ~makeSymbol(
        shape = CONTOUR$LEGEND$lin$shape,
        width = 24,
        fillColor = "white",
        fillOpacity = 0,
        color = .x,
        "stroke-width" = 5
      )
    )
    legend_sty <- purrr::imap_chr(
      CONTOUR$LEGEND$lin$labelStyle, 
      ~paste0(.y, ":", .x)
      ) %>% 
      paste(collapse = ";")
    
    lf %>% 
      clearImages() %>% 
      removeLayersControl() %>% 
      addPolylines(data = st_transform(contour_geom, 4326), 
                   label = ~label, dashArray = "10,10",
                   weight = 3, color = ~colour,
                   labelOptions = CONTOUR$labelOptions) %>% 
      addLegendImage(
        title = htmltools::tags$div(contour_to_plot$title, style = legend_sty),
        images = legend_symb,
        width = CONTOUR$LEGEND$lin$width,
        height = CONTOUR$LEGEND$lin$height,
        labels = legend_lab,
        position = CONTOUR$LEGEND$lin$position,
        labelStyle = legend_sty
      )
  }
  
  if(!is.null(contour_to_plot$plg) & !is.null(contour_to_plot$lin)){
    if(
      inherits(contour_to_plot$plg, "sf") && 
      all(st_geometry_type(contour_to_plot$plg) %in% c("POLYGON", "MULTIPOLYGON")) &&
      inherits(contour_to_plot$lin, "sf") && 
      all(st_geometry_type(contour_to_plot$lin) == "LINESTRING")
    ) {
      
      return(func_plg(lf) %>% func_lin())
      
    } else {
      
      message("Contour polygons and/or lines are not sf polygon / sf linestring object")
      return(lf)
      
    }
    
  } else if(is.null(contour_to_plot$lin)){
    if(
      inherits(contour_to_plot$plg, "sf") && 
      all(st_geometry_type(contour_to_plot$plg) %in% c("POLYGON", "MULTIPOLYGON"))
    ) {
      
      return(func_plg(lf))
      
    } else {
      
      message("Contour polygons are not sf polygon object")
      return(lf)
      
    }
    
  } else if(is.null(contour_to_plot$plg)){
    if(
      inherits(contour_to_plot$lin, "sf") && 
      all(st_geometry_type(contour_to_plot$lin) == "LINESTRING")
    ) {
      
      return(func_lin(lf))
    } else {
      
      message("Contour lines are not sf linestring object")
      return(lf)
      
    }
  }
  
}
